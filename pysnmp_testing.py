from pysnmp import hlapi
import pysnmp_functions

#setting the hostname of the remote device to "SNMPHost"
#pysnmp_functions.set('192.168.1.204', {'1.3.6.1.2.1.1.5.0': 'SNMPHost'}, hlapi.CommunityData('cjr'))

#retrieving the hostname of the remote device
print(pysnmp_functions.get('192.168.1.204', ['1.3.6.1.2.1.1.5.0'], hlapi.CommunityData('cjr')))

#getting interface name and description for all interfaces 
#last parameter is OID containing number of interfaces so they can all be looped
its = pysnmp_functions.get_bulk_auto('192.168.1.204', ['1.3.6.1.2.1.2.2.1.2', '1.3.6.1.2.1.3.1.1.1.1.1.8'], hlapi.CommunityData('cjr'), '1.3.6.1.2.1.2.1.0')

#printing results in OID = value format
for it in its:
    for k, v in it.items():
        print("{0} = {1}".format(k, v))
    #leaving blank line for output of each interface 
    print('')

